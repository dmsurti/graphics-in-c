#include <graphics.h>

int main(int argc, char * argv[])
{
   int gd, gm;
   int x, y;
    
   gd = DETECT;
   initgraph (&gd, &gm, "");
   x = getmaxx ();
   y = getmaxy ();

  
   setcolor(WHITE);
   rectangle(x / 30, y / 20, x / 5, y / 4);
   outtextxy(x / 30 + 15, y / 8 + 5, "Rectangle");

   delay(5000);
   closegraph ();
   return 0;
}
