This is a reincarnation of all the graphics code I wrote in school and college days (1993 - 1999)
using Turbo C's BGI (Borland Graphics Interface). 

I recently discovered this code when unarchiving some very old backup files and was happy to find
that we have SDL based BGI emulator and so have tried to revive this using the [SDL_bgi library,
brought to you by Guido Gonzato](https://sdl-bgi.sourceforge.io)

## Pre Requisites

Ensure that you have SDL and SDL_bgi installed by following the instructions in `Install_*.md`
document for your platform after downloading [SLD_bgi](https://sdl-bgi.sourceforge.io).

## Instructions

- Clone this directory.

- Navigate to an example directory under test and build the example using provided make file.

```
cd test/rect
make
```

- Run the example from the bin directory.

```
./bin/rect
```

## Examples

- test/rect.c : drawing a rectangle.

### Disclaimer

I claim no original authorship of the programs in this repository as I wrote (copied/adapted) these
from multiple textbooks I was learning graphics from.

I have re-used the build system from SDL_bgi repository to compile and run this adapted graphics
code.
